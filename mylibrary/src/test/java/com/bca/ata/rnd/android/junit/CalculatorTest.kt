package com.bca.ata.rnd.android.junit

import com.bca.ata.rnd.android.junit.kotlin.Calculator
import org.junit.Before
import org.junit.Test


class CalculatorTest{
    lateinit var classToTest: Calculator

    @Before
    fun setUp() {
        classToTest = Calculator()
    }

    @Test
    fun calculate() {
        assert(classToTest.calculate(2, 2) == 4)
    }

}