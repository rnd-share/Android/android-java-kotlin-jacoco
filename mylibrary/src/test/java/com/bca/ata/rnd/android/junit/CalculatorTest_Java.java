package com.bca.ata.rnd.android.junit;

import org.junit.*;

import com.bca.ata.rnd.android.junit.java.MyLibCalculatorJava;

public class CalculatorTest_Java {
    public MyLibCalculatorJava classToTest;

    @Before
    public void setUp() {
        classToTest = new MyLibCalculatorJava();
    }

    @Test
    public void calculate() {
        assert (classToTest.calculate(2, 2) == 4);
    }

}
