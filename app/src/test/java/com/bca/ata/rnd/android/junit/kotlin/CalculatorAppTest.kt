package com.bca.ata.rnd.android.junit.kotlin

import org.junit.Before
import org.junit.Test


class CalculatorAppTest{
    lateinit var calculatorApp: CalculatorApp

    @Before
    fun setUp() {
        calculatorApp = CalculatorApp()
    }

    @Test
    fun calculate() {
        assert(calculatorApp.calculate(2, 2) == 4)
    }

}