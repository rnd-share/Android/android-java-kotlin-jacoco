package com.bca.ata.rnd.android.junit.java;

import org.junit.*;

public class CalculatorAppTest {
    private CalculatorApp classToTest;

    @Before
    public void setUp() {
        classToTest = new CalculatorApp();
    }

    @Test
    public void calculate() {
        assert (classToTest.calculate(2, 2) == 4);
    }

}
